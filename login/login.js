angular.module('1d.ui.login', ['ngRoute'])
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/login', {
				controller: 'LoginCtrl',
				templateUrl: 'login/login.tpl.html',
				controllerAs: 'ctrl',
				title: 'Login'
			});
	}])
	.controller('LoginCtrl', ['$rootScope', '$http', '$location', '$route',
		function LoginCtrl($rootScope, $http, $location, $route) {

			var self = this;

			self.tab = function (route) {
				return $route.current && route === $route.current.controller;
			};

			var authenticate = function (credentials, callback) {


				$http({
					method: 'POST',
					url: 'http://localhost:8080/j_spring_security_check',
					data: credentials,
					transformRequest: function (data) {
						var str = [];
						for (var p in data)
							str.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
						return str.join("&");
					},
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).then(function (response) {
					if (response.data.status = 200) {
						$rootScope.authenticated = true;
					} else {
						$rootScope.authenticated = false;
					}
					callback && callback();
				}, function () {
					$rootScope.authenticated = false;
					callback && callback();
				});

			};

			// authenticate();

			self.credentials = {};

			self.login = function () {
				authenticate(self.credentials, function () {
					if ($rootScope.authenticated) {
						$location.path("/");
						self.error = false;
					} else {
						$location.path("/login");
						self.error = true;
					}
				});
			};

			self.logout = function () {
				$http.post('http://localhost:8080/logout', {}).finally(function () {
					$rootScope.authenticated = false;
					$location.path("/");
				});
			}
		}]);


