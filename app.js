'use strict';
angular.module('1d.ui', [
    'ui.bootstrap',
    'ngRoute',
    '1d.ui.login',
    '1d.ui.home'
]).constant('Constants', {
}).config(['$httpProvider', '$routeProvider', function ($httpProvider, $routeProvider) {
    // $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
      $httpProvider.defaults.withCredentials = true;
      $httpProvider.defaults.useXDomain = true; 
    $httpProvider.defaults.headers.common = {};
    $routeProvider.otherwise({redirectTo: '/'});
}])
    .run(['$rootScope', '$location', '$window', '$routeParams', 'Constants',
        function ($rootScope, $location, $window, $routeParams, Constants) {
            $rootScope.go = function (path) {
                $location.path(path);
            };
        }]);
   