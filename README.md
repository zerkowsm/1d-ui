# 1d-ui
---

## Pre-requisites
* Nodejs ( suggested v4.4.5)

## Setup:

```
$ npm install -g http-server
```

## Run:
```
$ http-server .
```

available parameters:

    -p 8082