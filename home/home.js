angular.module('1d.ui.home', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeCtrl',
                templateUrl: 'home/home.tpl.html',
                controllerAs: 'ctrl',
                title: 'Home'
            });
    }])
    .factory('HomeService', ['$http', '$q', function ($http, $q) {
        return {
            getCurrentUserDiscounts: function () {
                    var deferred = $q.defer();
                                       
                    // $http({
                    //     method: 'GET',
                    //     url: 'http://localhost:8080/api/customer/discounts/customer/current',
                    //     headers: {'Content-Type': 'application/json'}})


                    $http.get('http://localhost:8080/api/customer/discounts/customer/current')
                    .then(function (reply) {
                        deferred.resolve(reply);
                    }).catch(function (reply) {
                        deferred.reject(reply);
                    });
                    return deferred.promise;
            }
        };
    }])
    .controller('HomeCtrl', ['$rootScope', '$http', '$location', 'HomeService',
        function HomeCtrl($rootScope, $http, $location, HomeService) {

            var self = this;  

            HomeService.getCurrentUserDiscounts().then(function (reply) {
                self.discounts = reply.data.content;
            }).catch(function (reply) {
            });   
        }]);

